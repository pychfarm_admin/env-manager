use std::process::Command;

fn set_java_home(java_home: &str) {
    let command = Command::new("setx")
        .args(&["JAVA_HOME", java_home])
        .output()
        .expect("Failed to set JAVA_HOME");

    println!("Setting JAVA_HOME: {:?}", command.status);
}

fn set_path_with_java_home(java_home: &str) {
    let command = Command::new("setx")
        .args(&["PATH", &format!("%PATH%;%{}%\\bin", java_home)])
        .output()
        .expect("Failed to set PATH");

    println!("Setting PATH with JAVA_HOME: {:?}", command.status);
}


fn remove_path_entry(entry: &str) {
    // Get the current PATH variable
    let current_path = std::env::var("PATH").expect("Failed to get PATH variable");

    // Split the PATH into individual entries
    let path_entries: Vec<&str> = current_path.split(';').collect();

    // Filter out the entry to be removed
    let new_path: Vec<&str> = path_entries.into_iter().filter(|&e| e != format!(r"%{}%\bin",entry)).collect();

    // Join the entries back into a single string
    let new_path_string = new_path.join(";");

    // Set the new PATH variable
    let command = Command::new("setx")
        .args(&["PATH", &new_path_string])
        .output()
        .expect("Failed to update PATH");

    println!("Removing entry from PATH: {:?}", command.status);
}


fn main() {

        if cfg!(windows) {
            println!("This code is being compiled for Windows.");
        } else {
            println!("This code is not being compiled for Windows.");
        }


    set_java_home("C:\\Users\\Administrator\\.jdks\\openjdk-19.0.1");
    set_path_with_java_home("JAVA_HOME");

    // remove_path_entry("JAVA_HOME");


    println!("Operations completed.");
}

#[cfg(test)]
mod tests {
    use env_cli::env_manager::{EnvironmentManager, EnvManagerTrait};

    #[test]
    fn test_add_environment() {
        // Create an EnvironmentManager instance
        let mut env_manager = EnvironmentManager { env_params: Vec::new() };

        // Add an environment

        // Retrieve the list of environments
        let environments = env_manager.list_all(true);

        env_manager.add("alias1".to_string(), "la1".to_string(), "path1".to_string());

        println!("========添加后========");
        env_manager.list_all(true);
        println!("========删除后========");
        env_manager.delete("JDK8".to_string());
        env_manager.list_all(true);
        println!("==========json===========");
        env_manager.write_config_json();
        env_manager.use_alias("JDK8".to_string());
        env_manager.write_config_json();
    }
    #[test]
    fn delete_test() {
        let mut env_manager = EnvironmentManager { env_params: Vec::new() };
        env_manager.delete("JDK8".to_string());
    }

    #[test]
    fn using_test() {
        let mut env_manager = EnvironmentManager { env_params: Vec::new() };
        env_manager.list_all(false);

        env_manager.use_alias("jdk8".to_string());
    }
}
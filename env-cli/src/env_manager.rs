use std::env;
use std::process::Command;

use prettytable::{Cell, Row, Table};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::init::{detect_shell, EnvManager, get_home_directory, write_string_to_file};

pub trait EnvManagerTrait {
    /// 添加环境
    fn add(&mut self, alias: String, la: String, path: String);

    /// 删除方法
    fn delete(&mut self, alias: String);
    /// 所有配置集合
    fn list_all(&mut self, print: bool);

    fn write_config_json(&self);

    fn use_alias(&mut self, alias: String);

    fn source_env(&mut self);
}


pub struct EnvironmentManager {
    pub  env_params:  Vec<AddEnvParam>,

}


impl EnvManagerTrait for EnvironmentManager {

    fn add(&mut self, alias: String, la: String, path: String) {
        let uid = Uuid::new_v4();

        // Create a new AddEnvParam instance
        let new_env = AddEnvParam {
            uid: uid.to_string(),
            la,
            path,
            alias,
            usingss: false,
        };

        self.env_params.push(new_env);
        self.write_config_json();
    }

    fn delete(&mut self, alias: String) {
        // Find the index of the environment with the specified alias
        if let Some(index) = self.env_params.iter().position(|env| env.alias == alias) {
            // Remove the environment at the found index
            self.env_params.remove(index);
            self.write_config_json();

        } else {
            // Handle the case where the environment with the specified alias is not found
            println!("Environment with alias '{}' not found.", alias);
        }
    }
    // fn list_all(&mut self) {
    //     if self.env_params.is_empty() {
    //         self.env_params = read_config_json();
    //     }
    //
    //     if self.env_params.is_empty() {
    //         println!("No environments configured.");
    //     } else {
    //         println!("List of all environments:");
    //         for env in &self.env_params {
    //             println!("UID: {}", env.uid);
    //             println!("Language: {}", env.la);
    //             println!("Path: {}", env.path);
    //             println!("Alias: {}", env.alias);
    //             println!("Using SS: {}", env.usingss);
    //             println!(); // Add a newline for better formatting
    //         }
    //     }
    // }

    fn list_all(&mut self, print: bool) {
        if self.env_params.is_empty() {
            self.env_params = read_config_json();
        }

        if print {
            if self.env_params.is_empty() {
            println!("No environments configured.");
        } else {
            let mut table = Table::new();
            table.add_row(Row::new(vec![
                Cell::new("UID"),
                Cell::new("Language"),
                Cell::new("Path"),
                Cell::new("Alias"),
                Cell::new("Using"),
            ]));

            for env in &self.env_params {
                table.add_row(Row::new(vec![
                    Cell::new(&env.uid),
                    Cell::new(&env.la),
                    Cell::new(&env.path),
                    Cell::new(&env.alias),
                    Cell::new(&env.usingss.to_string()), // Convert bool to string
                ]));
            }


                table.printstd();
        }
        }
    }
    fn write_config_json(&self)
    {
        let json = serde_json::to_string(&self.env_params).expect("Failed to serialize to JSON");
        write_config_json(json.as_str());
    }

    fn use_alias(&mut self, alias: String) {
        // Find the index of the environment with the specified alias
        if let Some(index) = self.env_params.iter().position(|env| env.alias == alias) {
            // Set the 'usingss' flag for the environment with the specified alias to true
            self.env_params[index].usingss = true;

            // Set the 'usingss' flag to false for other environments with the same language
            for i in 0..self.env_params.len() {
                if i != index && self.env_params[i].la == self.env_params[index].la {
                    self.env_params[i].usingss = false;
                }
            }

            self.write_config_json();
            self.source_env();

            println!("Environment with alias '{}' is now in use. Other environments with the same language are set to 'usingss: false'.", alias);
        } else {
            // Handle the case where the environment with the specified alias is not found
            println!("Environment with alias '{}' not found.", alias);
        }
    }
    fn source_env(&mut self){

        let mut env_vec: Vec<EnvManager> = Vec::new();

        for x in &self.env_params {
            if x.usingss {
                let name = crate::init::EnvKeyName::from_la(x.la.as_str());
                env_vec.push(EnvManager::new(name, x.path.as_str()));
            };
        }




        let mut all_results = String::new();

        for x in &env_vec {
            // 将 source_string 结果追加到字符串
            all_results.push_str(&x.source_string());
        }



        if cfg!(windows){
            for x in &env_vec {


                set_java_home(x.name.to_lag_home(),x.value.as_str());
                set_path_with_java_home(x.name.to_lag_home());
            }
        } else {
            if let Ok(home_directory) = get_home_directory().ok_or("无法获取 HOME 目录") {
                if let Err(err) = write_string_to_file(format!("{}/env_manager/customer", home_directory).as_str(), all_results.as_str()) {
                    eprintln!("Error: {}", err);
                } else {
                    println!("String written to file successfully.");
                }
            };


        }
    }

}


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddEnvParam {
    pub uid: String,
    pub la: String,
    pub path: String,
    pub alias: String,
    pub usingss: bool,      // 表示路径是否正在使用
}




/// 读取配置文件
pub fn read_config_json() -> Vec<AddEnvParam> {
    if let Ok(home_directory) = get_home_directory().ok_or("无法获取 HOME 目录") {
        let config_path = format!("{}/env_manager/config.json", home_directory);
        if let Ok(file_contents) = crate::init::read_file_content(config_path.as_str()) {
            if let Ok(parsed_data) = serde_json::from_str::<Vec<AddEnvParam>>(&file_contents) {
                return parsed_data;
            } else {
                eprintln!("Error parsing JSON data from the config file");
            }
        } else {
            eprintln!("Error reading file content from the config file");
        }
    }
    Vec::new() // Return an empty vector if there's any error
}

/// 写配置文件
pub fn write_config_json(data: &str) {
    if let Ok(home_directory) = crate::init::get_home_directory().ok_or("无法获取 HOME 目录") {
        crate::init::write_string_to_file(format!("{}/env_manager/config.json", home_directory).as_str(), data).expect("写入配置文件失败");
    }
}

fn set_java_home(name :&str,java_home: &str) {
    let command = Command::new("setx")
        .args(&[name, java_home])
        .output()
        .expect("Failed to set JAVA_HOME");

    println!("Setting JAVA_HOME: {:?}", command.status);
}

fn set_path_with_java_home(java_home: &str) {
    let command = Command::new("setx")
        .args(&["PATH", &format!("%PATH%;%{}%\\bin", java_home)])
        .output()
        .expect("Failed to set PATH");

    println!("Setting PATH with JAVA_HOME: {:?}", command.status);
}

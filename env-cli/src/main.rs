use serde::{Deserialize, Serialize};
use structopt::StructOpt;

use crate::init::init_pj;
use env_cli::env_manager::{EnvironmentManager, EnvManagerTrait};

pub mod env_manager;
pub mod init;

/// 一个简单的命令行工具，用于处理环境相关的操作。
#[derive(StructOpt, Debug, Serialize, Deserialize)]
enum Command {
    /// 使用现有的环境
    #[structopt(name = "use")]
    Use {
        /// 别名
        #[structopt(short = "a", long = "alias")]
        alias: String,
    },

    /// 添加新的环境
    #[structopt(name = "add")]
    Add {
        /// 别名
        #[structopt(short = "a", long = "alias")]
        alias: String,
        /// 开发语言
        #[structopt(short = "l", long = "la")]
        la: String,
        /// 文件地址
        #[structopt(short = "p", long = "path")]
        path: String,
    },

    /// 删除环境
    #[structopt(name = "delete")]
    Delete {
        /// 别名
        #[structopt(short = "a", long = "alias")]
        alias: String,
    },

    /// 显示环境列表
    #[structopt(name = "list")]
    List,
}

/// 用于处理环境相关操作的命令行工具。
#[derive(StructOpt, Debug, Serialize, Deserialize)]
struct MyArgs {
    #[structopt(subcommand)]
    command: Command,
}

fn main() {
    init_pj();

    let args = MyArgs::from_args();
    let mut env_manager = EnvironmentManager { env_params: Vec::new() };

    env_manager.list_all(false);
    match args.command {
        Command::Use { alias } => {
            // 处理 "env-cli use" 命令
            println!("Using environment with alias: {}", alias);
            env_manager.use_alias(alias);
        }
        Command::Add { alias, la, path } => {
            // 处理 "env-cli add" 命令
            println!(
                "Adding environment with alias: {}, language: {}, and path: {}",
                alias, la, path
            );
            env_manager.add(alias, la, path);
        }

        Command::Delete { alias } => {
            env_manager.delete(alias);
        }
        Command::List => {
            env_manager.list_all(true);
        }
    }
}


